import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraderFollowUpPage } from './trader-follow-up.page';

describe('TraderFollowUpPage', () => {
  let component: TraderFollowUpPage;
  let fixture: ComponentFixture<TraderFollowUpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraderFollowUpPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraderFollowUpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
