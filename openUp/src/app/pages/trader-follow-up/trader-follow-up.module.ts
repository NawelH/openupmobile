import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TraderFollowUpPage } from './trader-follow-up.page';

const routes: Routes = [
  {
    path: '',
    component: TraderFollowUpPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TraderFollowUpPage]
})
export class TraderFollowUpPageModule {}
