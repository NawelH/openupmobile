import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanQrCodeTraderPage } from './scan-qr-code-trader.page';

describe('ScanQrCodeTraderPage', () => {
  let component: ScanQrCodeTraderPage;
  let fixture: ComponentFixture<ScanQrCodeTraderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanQrCodeTraderPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanQrCodeTraderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
