import { Component, OnInit } from '@angular/core';
import {  DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-datas',
  templateUrl: './datas.page.html',
  styleUrls: ['./datas.page.scss'],
})
export class DatasPage implements OnInit {

  accounts;
  first;
  
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getDataAccounts().then(data => {
      console.log(data['hydra:member']);
      this.accounts = data['hydra:member'];
      this.first = this.accounts[0];
    })
    .catch(error => {
      console.log(error.status);
      console.log(error.error); // error message as string
      console.log(error.headers);
    });
  }
}