import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasPage } from './datas.page';

describe('DatasPage', () => {
  let component: DatasPage;
  let fixture: ComponentFixture<DatasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
