import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThanksTraderPage } from './thanks-trader.page';

describe('ThanksTraderPage', () => {
  let component: ThanksTraderPage;
  let fixture: ComponentFixture<ThanksTraderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThanksTraderPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThanksTraderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
