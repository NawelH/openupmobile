import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ThanksTraderPage } from './thanks-trader.page';

const routes: Routes = [
  {
    path: '',
    component: ThanksTraderPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ThanksTraderPage]
})
export class ThanksTraderPageModule {}
