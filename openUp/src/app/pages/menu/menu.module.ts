import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      { 
        path: 'profil', 
        loadChildren: '../profil/profil.module#ProfilPageModule' 
      },
      { 
        path: 'status', 
        loadChildren: '../status/status.module#StatusPageModule' 
      },
      { 
        path: 'historical', 
        loadChildren: '../historical/historical.module#HistoricalPageModule' 
      },
      { 
        path: 'advantages', 
        loadChildren: '../advantages/advantages.module#AdvantagesPageModule' 
      }
    ]
  }
  // ,
  // { 
  //   path: '', 
  //   redirectTo: '/profil'
  // }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
