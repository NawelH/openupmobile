import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  firstname = 'Lisa';
  lastname = 'Liu';
  img = "https://randomuser.me/api/portraits/women/50.jpg";

  // redirection pages of menu
  pages = [
    {
      title: 'Profil',
      url: '/profil'
    },
    {
      title: 'Accueil',
      url: '/status'
    },
    {
      title: 'Historique',
      url: '/historical'
    },
    {
      title: 'Mes avantages',
      url: '/advantages'
    }
  ];

  selectedPath = '';

  constructor(private router: Router, private authService: AuthenticationService) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }

}
