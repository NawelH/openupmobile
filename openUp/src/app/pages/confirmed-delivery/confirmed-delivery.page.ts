import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirmed-delivery',
  templateUrl: './confirmed-delivery.page.html',
  styleUrls: ['./confirmed-delivery.page.scss'],
})
export class ConfirmedDeliveryPage {

  status: string = 'trader';

  constructor(public navCtrl: NavController, private launchNavigator: LaunchNavigator, private router: Router) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  // navigate to google map, take your position and give the path to the address 
  navme(address) {
    this.launchNavigator.navigate(address);
  }

  backOrder() {
    // return to list proximity available orders
    this.router.navigate(['proximity-deliveries'])
  }

  // give class="trader" if status === 'trader', and hide the button how redirect to the address
  setClass() {
    let myClass = {
      trader: this.status === 'trader'
    }
    return myClass;
  }

}

