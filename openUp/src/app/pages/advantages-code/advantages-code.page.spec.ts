import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvantagesCodePage } from './advantages-code.page';

describe('AdvantagesCodePage', () => {
  let component: AdvantagesCodePage;
  let fixture: ComponentFixture<AdvantagesCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvantagesCodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvantagesCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
