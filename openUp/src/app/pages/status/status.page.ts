import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-status',
  templateUrl: './status.page.html',
  styleUrls: ['./status.page.scss'],
})
export class StatusPage implements OnInit {

  constructor(private router: Router) { }

  // update status user to 'deliveryman' and redirect to the proximity-deliveries page
  deliveryManStatus() {
    this.router.navigate(['/proximity-deliveries']);
  }

  // update status user to 'orderer' and redirect to the shop-categories page
  ordererStatus() {
    this.router.navigate(['/shop-categories']);
  }
  ngOnInit() {
  }

}