import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qr-code-for-trader',
  templateUrl: './qr-code-for-trader.page.html',
  styleUrls: ['./qr-code-for-trader.page.scss'],
})
export class QrCodeForTraderPage implements OnInit {

  img = 'assets/img/qr-code.png';

  constructor() { }

  ngOnInit() {
  }

}
