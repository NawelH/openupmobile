import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QrCodeForTraderPage } from './qr-code-for-trader.page';

const routes: Routes = [
  {
    path: '',
    component: QrCodeForTraderPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [QrCodeForTraderPage]
})
export class QrCodeForTraderPageModule {}
