import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {

  img = "https://randomuser.me/api/portraits/women/50.jpg";
  clickCounter: number = 0;
  data;
  orders = [];
  ordersByOrderer;
  ordersByDeliverer;


  constructor(private router: Router, private menuCtrl: MenuController,
    private dataService: DataService, private orderService: OrdersService ) { }

  return() {
    //this.router.navigate(['/menu']);
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  countClick() {
    // add 1 like 
    if (this.clickCounter === 0) {
      this.clickCounter += 1;
      // add request => update database add 1 like on click
    }
    else {
      // remove 1 like
      this.clickCounter -= 1;
      // add request => update database substract 1 like on click
    }
  }

  //  modifie la class en fonction du click
  setClasses() {
    let myClasses = {
      fullHeart: this.clickCounter > 0,
      emptyHeart: this.clickCounter <= 0,
    }
    return myClasses;
  }

  ngOnInit() {
    this.data = this.dataService.getAccount();
    //console.log(this.data);
    //console.log(this.data.id);

    //MODIFICATIONS//
    const ids = [-1];
    
    this.dataService.getOrders().then(res => {
      //console.log(res);
      for (let i = 0; i < Object.values(res).length; i++) {
        // si l'id de la commande est dans le tableau ids on passe, sinon on ajoute l'id de la commande a ids et la commande a this.orders
        if (ids.indexOf(res[i].id) === -1) {
          // @ts-ignore
          ids.push(res[i].id);
          this.orders.push(res[i]);
          
          // nombre commande user
          let id = res[i].walletId;
          this.orderService.getOrdersByOrderers(id).then(result => {
            res[i].user_info = result;
            this.ordersByOrderer = result;
            //console.log(result);

          })
          //.then(r => console.log(res[i]))

          // nombre livraison user
          let userId = res[i].deliveryManId;
          //console.log(userId);
          this.orderService.getOrdersByDeliverer(userId).then(results => {
            res[i].user_info = results;
            this.ordersByDeliverer = results;
            //console.log(results);

          })
        
        }
        //console.log(this.orders);
      }

    });

    
  }

}
