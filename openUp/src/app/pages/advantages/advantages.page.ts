import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-advantages',
  templateUrl: './advantages.page.html',
  styleUrls: ['./advantages.page.scss'],
})
export class AdvantagesPage implements OnInit {

  img = "https://randomuser.me/api/portraits/women/50.jpg";
  firstname = "Lisa";
  lastname = "Liu"
  codeAdvantages = "Qx98j5ertZO";

  constructor(private router: Router) { }

  advantages() {
    this.router.navigate(['/redirect-town-hall']);
  }

  return() {
    this.router.navigate(['/profil']);
  }

  ngOnInit() {
  }

}