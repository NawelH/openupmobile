import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvantagesPage } from './advantages.page';

describe('AdvantagesPage', () => {
  let component: AdvantagesPage;
  let fixture: ComponentFixture<AdvantagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvantagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvantagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
