import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email;
  password = this.password;
  role;

  constructor(private router: Router, private authService: AuthenticationService,
    private dataService: DataService) {
  }

  ngOnInit() {
  }

  // loginChoice() {
  //   this.router.navigate(['/login/choice']);
  // }

  // login fonction
  login() {
    const form = {
      email: this.email,
      password: this.password,
      role: this.role
    }
    //console.log(form);

    this.dataService.login(form).then(res => {
      //console.log(res);
      this.authService.login();
      this.dataService.getDataAccount(this.email).then(res => {
        console.log(res);

        let account = res[0].account;
        console.log(account);
        var array = account.split('/');
        console.log(array);
        let idAccount = array[3];
        console.log(idAccount);

        localStorage.setItem("id", idAccount);

        this.dataService.getData(idAccount).then(res => {
          this.dataService.setAccount(res);
          //console.log(this.dataService.getAccount());
          this.router.navigate(['/profil']);

        }).catch(error => {
          //console.log(error);
        });

      });

    }).catch(reason => console.log(reason));

  }

  // redirect to register particular page
  register() {
    this.router.navigate(['/register']);
  }

  // redirect to register trader page
  registerTrader() {
    this.router.navigate(['/register-trader']); 
  }

}
