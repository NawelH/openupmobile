import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payments-trader',
  templateUrl: './payments-trader.page.html',
  styleUrls: ['./payments-trader.page.scss'],
})
export class PaymentsTraderPage implements OnInit {

  amountOrders = 479;
  orders = ['10', '27', '39', '151', '375', '1', '2', '5', '7', '8', '45'];
  id = '457';
  status = 'en cours';
  firstname = 'Lisa';
  lastname = 'Liu';
  amount = 15;
  constructor() { }

  ngOnInit() {
  }

}
