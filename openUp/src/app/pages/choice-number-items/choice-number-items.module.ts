import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChoiceNumberItemsPage } from './choice-number-items.page';

const routes: Routes = [
  {
    path: '',
    component: ChoiceNumberItemsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChoiceNumberItemsPage]
})
export class ChoiceNumberItemsPageModule {}
