import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-choice-number-items',
  templateUrl: './choice-number-items.page.html',
  styleUrls: ['./choice-number-items.page.scss'],
})
export class ChoiceNumberItemsPage implements OnInit {

  item: string = 'pain';
  shop: string = 'le bon pain';
  categorie: string = 'boulangerie';
  items: string = 'baguettes';
  numberItems: string = '';

  constructor(private router: Router, private toastCtrl: ToastController) { }

  async continue() {
    if (this.numberItems.length > 2 ) {  
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer un nombre inférieur',
        duration: 2000
      });
      toast.present();
    }
  }

  return() {
    
  }

  ngOnInit() {
  }

}
