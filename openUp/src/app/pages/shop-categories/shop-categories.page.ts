import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shop-categories',
  templateUrl: './shop-categories.page.html',
  styleUrls: ['./shop-categories.page.scss'],
})
export class ShopCategoriesPage implements OnInit {

  // Les catégories de shops
  chocolats: string = '../assets/img/chocolats.jpeg';
  epices: string = '../assets/img/epices.jpeg';
  fromages: string = '../assets/img/fromages.jpg';
  fruitLegumes: string = '../assets/img/fruits-legumes.webp';
  pain: string = '../assets/img/pain.jpeg';
  viande: string = '../assets/img/viande.jpeg';

  shopCatPics = { "Chocolats": this.chocolats, "Epices": this.epices, "Fromages": this.fromages, "Fruits / Légumes": this.fruitLegumes, "Pain": this.pain, "Viande": this.viande };
  
  // Les liens vers les pages de boutiques par catégorie - A faire!



  constructor(private router: Router ) { }

  ngOnInit() {
  }

  /**
   * showBoutics
   * Redirige vers la page correspondant à la catégorie de boutique sélectionnée
   */
  public showBoutics(shopCat: any) {
    switch (shopCat) {
      case "Chocolats":
        this.router.navigate(['/register']);
        break;
      case "Epices":
        this.router.navigate(['/register']);
        break;
      case "Pain":
        this.router.navigate(['/register']);
        break;
      case "Fromages":
        this.router.navigate(['/register']);
        break;
      case "Fruits / Légumes":
        this.router.navigate(['/register']);
        break;
      case "Viande":
        this.router.navigate(['/register']);
        break;
    
      default:
        break;
    }
  }
}
