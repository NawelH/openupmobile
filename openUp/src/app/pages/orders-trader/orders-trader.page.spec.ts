import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersTraderPage } from './orders-trader.page';

describe('OrdersTraderPage', () => {
  let component: OrdersTraderPage;
  let fixture: ComponentFixture<OrdersTraderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersTraderPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersTraderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
