import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orders-trader',
  templateUrl: './orders-trader.page.html',
  styleUrls: ['./orders-trader.page.scss'],
})
export class OrdersTraderPage implements OnInit {

  deliveries= ['un', 'deux', 'trois'];
  firstname: string = 'Lisa';
  lastname: string = 'Liu';
  numberItems: number = 5;
  items: string = 'baguettes';
  img: string = 'https://randomuser.me/api/portraits/women/50.jpg';

  constructor() { }

  ngOnInit() {
  }

}
