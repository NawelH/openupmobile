import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanQrCodeOrdererPage } from './scan-qr-code-orderer.page';

describe('ScanQrCodeOrdererPage', () => {
  let component: ScanQrCodeOrdererPage;
  let fixture: ComponentFixture<ScanQrCodeOrdererPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanQrCodeOrdererPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanQrCodeOrdererPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
