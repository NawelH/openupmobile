import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ScanQrCodeOrdererPage } from './scan-qr-code-orderer.page';
//import { NgxQRCodeModule } from 'ngx-qrcode2';

const routes: Routes = [
  {
    path: '',
    component: ScanQrCodeOrdererPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
    //NgxQRCodeModule
  ],
  declarations: [ScanQrCodeOrdererPage]
})
export class ScanQrCodeOrdererPageModule {}
