import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.page.html',
  styleUrls: ['./shop.page.scss'],
})
export class ShopPage implements OnInit {

 
  // Tableau listant les details à afficher - from API
  // Ce tableau devra contenir x éléments: nomBoutique, adresse, et pour chaque produit(nomProduit, prix, imageProduit)
  shopData = [
    {
      name: "Boulangerie Manue", address: "56 Blvd Frederic Mistral", img: "../assets/img/pain.jpeg"
    }];

  shopProducts = [
    {
      name: "Ciabatta",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Pain aux olives",
      prix: "12€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Pain aux chocolats",
      prix: "25€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Caca",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "pipi",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "zozo",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Croissants",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Blabla",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Ciabatta",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Ciabatta",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Ciabatta",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Ciabatta",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Ciabatta",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Ciabatta",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    },
    {
      name: "Ciabatta",
      prix: "15€",
      imgProduit: "../assets/img/pain.jpeg"
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
