import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterIdCardPage } from './register-id-card.page';

describe('RegisterIdCardPage', () => {
  let component: RegisterIdCardPage;
  let fixture: ComponentFixture<RegisterIdCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterIdCardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterIdCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
