import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accepted-payment',
  templateUrl: './accepted-payment.page.html',
  styleUrls: ['./accepted-payment.page.scss'],
})
export class AcceptedPaymentPage implements OnInit {

  constructor(private router: Router) { }

  ok() {
    this.router.navigate(['confirmed-order'])
  }

  ngOnInit() {
  }

}
