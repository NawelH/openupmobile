import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AcceptedPaymentPage } from './accepted-payment.page';

const routes: Routes = [
  {
    path: '',
    component: AcceptedPaymentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AcceptedPaymentPage]
})
export class AcceptedPaymentPageModule {}
