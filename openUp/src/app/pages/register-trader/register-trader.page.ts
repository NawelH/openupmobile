import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

//import { PostProvider } from '../../providers/post-provider';
import { ToastController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-register-trader',
  templateUrl: './register-trader.page.html',
  styleUrls: ['./register-trader.page.scss'],
})
export class RegisterTraderPage implements OnInit {

  
  firstname;
  lastname;
  shop: string = "";
  password: string = "";
  confirm_password: string = "";
  email: string = "";
  phone: string = "";
  address: string = "";
  city;
  zipcode;
  siret: string = "";
  nameShop;
  type;

  private headers = {headers: new HttpHeaders  ({
    'Content-Type': 'application/json',
    // Authorization: Bearer ${this.token}
  })};

  constructor(
    private router: Router, 
    private toastCtrl: ToastController,
    private http: HttpClient ) { }

  ngOnInit() {
  }

  // recover and register data user to the bdd
  async signIn() {

    if(this.firstname === "") {
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer votre prénom',
        duration: 2000
      });
      toast.present();
    } 
    else if (this.lastname === "") {
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer votre nom',
        duration: 2000
      });
      toast.present();
    }
    else if (this.password === "") {
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer un mot de passe',
        duration: 2000
      });
      toast.present();
    }
    else if (this.confirm_password !== this.password ) {
      const toast = await this.toastCtrl.create({
        message: 'Veuillez renseigner le même mot de passe',
        duration: 2000
      });
      toast.present();
    } 
    // else if (this.email === "") {
    //   const toast = await this.toastCtrl.create({
    //     message: 'Veuillez entrer votre email',
    //     duration: 2000
    //   });
    //   toast.present();
    // }
    else if (this.phone.length !== 10) {  
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer un numéro valide',
        duration: 2000
      });
      toast.present();
    }
    else if (this.address === "" || this.city === "" || this.zipcode === "") {  
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer une adresse valide',
        duration: 2000
      });
      toast.present();
    }
    else if (this.siret.length !== 14) {  
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer votre numéro de siret',
        duration: 2000
      });
      toast.present();
    }
    else if (this.nameShop === "") {  
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer votre nom de commerce',
        duration: 2000
      });
      toast.present();
    }
    else {

       // register-trader to api 
       let postData = {
        firstname: this.firstname,
        lastname: this.lastname,
        email: this.email,
        role: 'trader',
        password: this.password,
        phone: this.phone,
        address: {
          address: this.address,
          city: this.city,
          zipcode: this.zipcode,
        },
        siret: this.siret,
        shop: {
          name: this.nameShop,
          // A TERMINER !!! shopType !!!
          shopType: "1"
        }
      }
      console.log(postData);

      this.http.post('http://127.0.0.1:8000/api/account', JSON.stringify(postData), this.headers )
      .subscribe(postData => {
        this.router.navigate(['/login']);
        console.log(postData['_body']);
      }, error => {
        console.log(error);
      });
  
      // this.postPvdr.postData(body, 'proses-api.php').subscribe(async data => {
      //   let alertmsg = data.msg;
      
      //   if(data.success) {
      //     this.router.navigate(['/login']);
      //     const toast = await this.toastCtrl.create({
      //       message: 'Register success',
      //       duration: 2000
      //     });
      //     toast.present();
      //   } else {
      //       const toast = await this.toastCtrl.create({
      //         message: 'alertmsg',
      //         duration: 2000
      //       });
      //   }
      // });
    }
  }
}
