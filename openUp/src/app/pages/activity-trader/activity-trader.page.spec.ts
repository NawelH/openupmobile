import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityTraderPage } from './activity-trader.page';

describe('ActivityTraderPage', () => {
  let component: ActivityTraderPage;
  let fixture: ComponentFixture<ActivityTraderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityTraderPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityTraderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
