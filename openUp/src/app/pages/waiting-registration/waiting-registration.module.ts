import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WaitingRegistrationPage } from './waiting-registration.page';

const routes: Routes = [
  {
    path: '',
    component: WaitingRegistrationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WaitingRegistrationPage]
})
export class WaitingRegistrationPageModule {}
