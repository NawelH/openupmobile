import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-to-confirm-order',
  templateUrl: './to-confirm-order.page.html',
  styleUrls: ['./to-confirm-order.page.scss'],
})
export class ToConfirmOrderPage implements OnInit {

  constructor(private router: Router) { }

  confirmOrder() {
    this.router.navigate(['/payment']);
  }
  ngOnInit() {
  }

}
