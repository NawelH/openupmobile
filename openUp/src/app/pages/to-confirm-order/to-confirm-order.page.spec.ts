import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToConfirmOrderPage } from './to-confirm-order.page';

describe('ToConfirmOrderPage', () => {
  let component: ToConfirmOrderPage;
  let fixture: ComponentFixture<ToConfirmOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToConfirmOrderPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToConfirmOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
