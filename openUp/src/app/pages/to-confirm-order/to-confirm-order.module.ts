import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ToConfirmOrderPage } from './to-confirm-order.page';

const routes: Routes = [
  {
    path: '',
    component: ToConfirmOrderPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ToConfirmOrderPage]
})
export class ToConfirmOrderPageModule {}
