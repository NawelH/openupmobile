import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  amount: string = '3,75';
  numberItems: number = 4;
  items: string = 'baguettes'

  constructor(private router: Router) { }

  continue() {
    this.router.navigate(['/accepted-payment'])
  }

  return() {
    this.router.navigate(['to-confirm-order']);
  }

  ngOnInit() {
  }

}
