import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-to-confirm-delivery',
  templateUrl: './to-confirm-delivery.page.html',
  styleUrls: ['./to-confirm-delivery.page.scss'],
})

export class ToConfirmDeliveryPage implements OnInit {

  orders = [];
  idOrder;
  ordersData;
  itemName;
  quantity;
  shopName;
  id;
  data;

  private headers = {headers: new HttpHeaders  ({
    'Content-Type': 'application/json',
    // Authorization: Bearer ${this.token}
  })};

  constructor(private router: Router, private dataService: DataService,
    private http: HttpClient ) { 
      //this.idOrder = this.route.snapshot.params.idOrder;
  }



  proximityDelivery() {
    this.router.navigate(['proximity-deliveries'])

  }

  ngOnInit() {

    //MODIFICATIONS

    // this.data = this.dataService.getAccount();
    // console.log(this.data.id);


    //FIN MODIFICATIONS

    const ids = [-1];

    this.dataService.getOrders().then(res => {
      //console.log(res);

          ids.push(res[0].id);
          this.orders.push(res[0]);
          
          let id = res[0].id;
          console.log(id);
          localStorage.setItem("idOrder", id);


          this.dataService.getDataAccountsUsers(id).then(result => {
            res[0].user_info = result;
            //console.log(result);
           
          }).then(r => console.log(res[0]))
          //console.log(res[0].itemName);

          this.itemName = res[0].itemName;
          this.quantity = res[0].quantity;
          this.shopName = res[0].shopName;


          // récupérer id user connecté
          // this.id = res[0].id;
        //    console.log(this.data.id);

        // console.log(idAccount);

          //console.log(this.ordersData);
        // }
        //console.log(this.orders);
      // }

     });
  }


  confirmeDelivery() {

    this.dataService.getOrders().then(res => {
      // id order
      let id = res[0].id;
      console.log(id);

      // id user
      this.data = this.dataService.getAccount();
      console.log(this.data.id);

      // update user-id in order table with the value of the user id
      
            
      //HELP
    this.http.post(`http://127.0.0.1:8000/api/order/${id}/accepted/${this.data.id}/`, this.headers )
    .subscribe(post => {
      this.router.navigate(['/delivery-details']);
      console.log(post);
    }, error => {
      console.log(error);
    });
  })


    // console.log(id);
    //this.router.navigate(['delivery-details'])
  }

 

}
