import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ToConfirmDeliveryPage } from './to-confirm-delivery.page';

const routes: Routes = [
  {
    path: '',
    component: ToConfirmDeliveryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ToConfirmDeliveryPage]
})
export class ToConfirmDeliveryPageModule {}
