import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginChoicePage } from './login-choice.page';

describe('LoginChoicePage', () => {
  let component: LoginChoicePage;
  let fixture: ComponentFixture<LoginChoicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginChoicePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginChoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
