import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-choice',
  templateUrl: './login-choice.page.html',
  styleUrls: ['./login-choice.page.scss'],
})
export class LoginChoicePage implements OnInit {

  constructor(private router: Router) { 

  }

  traderLogin() {
    this.router.navigate(['/login']);
  }

  particularLogin() {
    this.router.navigate(['/login']);
  }
  ngOnInit() {
  }

}