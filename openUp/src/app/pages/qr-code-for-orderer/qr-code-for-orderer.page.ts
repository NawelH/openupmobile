import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qr-code-for-orderer',
  templateUrl: './qr-code-for-orderer.page.html',
  styleUrls: ['./qr-code-for-orderer.page.scss'],
})
export class QrCodeForOrdererPage implements OnInit {

  img = 'assets/img/qr-code.png';
  firstname = 'lisa';
  lastname = 'liu';
  
  constructor() { }

  ngOnInit() {
  }

}
