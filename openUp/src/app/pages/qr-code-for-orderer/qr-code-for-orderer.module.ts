import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QrCodeForOrdererPage } from './qr-code-for-orderer.page';

const routes: Routes = [
  {
    path: '',
    component: QrCodeForOrdererPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [QrCodeForOrdererPage]
})
export class QrCodeForOrdererPageModule {}
