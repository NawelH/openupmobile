import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

//import { PostProvider } from '../../providers/post-provider';
import { ToastController } from '@ionic/angular';
import {  DataService } from 'src/app/services/data.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

 
  //username: string = "";
  password: string = "";
  confirm_password: string = "";
  email: string = "";
  phone: string = "";
  address: string = "";
  city;
  zipcode;
  firstname;
  lastname;

  private headers = {headers: new HttpHeaders  ({
    'Content-Type': 'application/json',
    // Authorization: Bearer ${this.token}
  })};

  constructor(
    private router: Router, 
   // private postPvdr: PostProvider,
    //public toastCtrl: ToastController,
    private toastCtrl: ToastController,
    private dataService: DataService,
    private http: HttpClient) { }

  ngOnInit() {
  }

  // recover and register data user to the bdd
  async signIn() {

    if(this.firstname==="") {
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer votre prénom',
        duration: 2000
      });
      toast.present();
    } 
    else if(this.lastname==="") {
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer votre nom',
        duration: 2000
      });
      toast.present();
    } 
    else if (this.password === "") {
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer un mot de passe',
        duration: 2000
      });
      toast.present();
    }
    else if (this.confirm_password !== this.password ) {
      const toast = await this.toastCtrl.create({
        message: 'Veuillez renseigner le même mot de passe',
        duration: 2000
      });
      toast.present();
    } 
    // else if (this.email === "") {
    //   const toast = await this.toastCtrl.create({
    //     message: 'Veuillez entrer votre email',
    //     duration: 2000
    //   });
    //   toast.present();
    // }
    else if (this.phone.length !== 10 ) {  
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer un numéro valide',
        duration: 2000
      });
      toast.present();
    }
    else if (this.address === "" ) {  
      const toast = await this.toastCtrl.create({
        message: 'Veuillez entrer une adresse',
        duration: 2000
      });
      toast.present();
    }
    else {

      // register to api 
      let postData = {
        firstname: this.firstname,
        lastname: this.lastname,
        email: this.email,
        role: 'user',
        password: this.password,
        phone: this.phone,
        address: {
        address: this.address,
        city: this.city,
        zipcode: this.zipcode,
        }
      }
      console.log(postData);

      this.http.post('http://127.0.0.1:8000/api/account', JSON.stringify(postData), this.headers )
      .subscribe(postData => {
        this.router.navigate(['/login']);
        console.log(postData['_body']);
      }, error => {
        console.log(error);
      });
      
    }
  }
}
