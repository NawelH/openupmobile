import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuTraderPage } from './menu-trader.page';

describe('MenuTraderPage', () => {
  let component: MenuTraderPage;
  let fixture: ComponentFixture<MenuTraderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuTraderPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuTraderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
