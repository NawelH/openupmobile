import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-menu-trader',
  templateUrl: './menu-trader.page.html',
  styleUrls: ['./menu-trader.page.scss'],
})
export class MenuTraderPage implements OnInit {

  shop = 'au pain';

  // redirection pages of menu trader
  pages = [
    {
      title: 'Commandes',
      url: '/orders-trader'
    },
    {
      title: 'Suivi activité',
      url: '/activity-trader'
    },
    {
      title: 'Boutique',
      url: '/shop'
    }
    
  ];

  selectedPath = '';

  constructor(private router: Router, private authService: AuthenticationService) { 
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }

}
