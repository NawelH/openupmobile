import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuTraderPage } from './menu-trader.page';

const routes: Routes = [
  {
    path: '',
    component: MenuTraderPage,
    children: [
      { 
        path: 'orders-trader', 
        loadChildren: '../orders-trader/orders-trader.module#OrdersTraderPageModule' 
      },
      { 
        path: 'activity-trader', 
        loadChildren: '../activity-trader/activity-trader.module#ActivityTraderPageModule' 
      },
      { 
        path: 'shop', 
        loadChildren: '../shop/shop.module#ShopPageModule' 
      }
    ]
  }
  // ,
  // { 
  //   path: '', 
  //   redirectTo: '/profil'
  // }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuTraderPage]
})
export class MenuTraderPageModule {}
