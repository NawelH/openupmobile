import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProximityDeliveriesPage } from './proximity-deliveries.page';

const routes: Routes = [
  {
    path: '',
    component: ProximityDeliveriesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProximityDeliveriesPage]
})
export class ProximityDeliveriesPageModule {}
