import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-proximity-deliveries',
  templateUrl: './proximity-deliveries.page.html',
  styleUrls: ['./proximity-deliveries.page.scss'],
})
export class ProximityDeliveriesPage implements OnInit {

  orders = [];

  img: string = 'https://randomuser.me/api/portraits/women/50.jpg';

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {

    // défini le tableau pour le tri des commandes (si une commande possède n items elle sera récupérée n fois, donc
    // pour l'affichage, on séléctionne uniquement les commandes distinctes
    const ids = [-1];

    this.dataService.getOrders().then(res => {
      //console.log(res);
      for (let i = 0; i < Object.values(res).length; i++) {
        // si l'id de la commande est dans le tableau ids on passe, sinon on ajoute l'id de la commande a ids et la commande a this.orders
        if (ids.indexOf(res[i].id) === -1) {
          // @ts-ignore
          ids.push(res[i].id);
          this.orders.push(res[i]);
          
          let id = res[i].deliveryId;
          this.dataService.getDataAccountsUsers(id).then(result => {
            res[i].user_info = result;
            //console.log(result);
           
          })
          //.then(r => console.log(res[i]))
        }
        //console.log(this.orders);
      }

    });
    
  }

  // id(idOrder) {
  //   localStorage.setItem("id", idOrder);

  //   this.dataService.getDataAccountsUsers(idOrder).then(res => {
  //     this.dataService.setAccount(res);
  //     console.log(this.dataService.getAccount());
  //     this.router.navigate(['/to-confirm-delivery/id/:id']);

  //   })

  // }

}
