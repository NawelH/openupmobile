import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectTownHallPage } from './redirect-town-hall.page';

describe('RedirectTownHallPage', () => {
  let component: RedirectTownHallPage;
  let fixture: ComponentFixture<RedirectTownHallPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectTownHallPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectTownHallPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
