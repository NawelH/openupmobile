import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RedirectTownHallPage } from './redirect-town-hall.page';

const routes: Routes = [
  {
    path: '',
    component: RedirectTownHallPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RedirectTownHallPage]
})
export class RedirectTownHallPageModule {}
