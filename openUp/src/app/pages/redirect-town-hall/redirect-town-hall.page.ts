import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-redirect-town-hall',
  templateUrl: './redirect-town-hall.page.html',
  styleUrls: ['./redirect-town-hall.page.scss'],
})
export class RedirectTownHallPage implements OnInit {

  constructor(private router: Router) { }

  return() {
    this.router.navigate(['/advantages-code']);
  }

  ngOnInit() {
  }

}
