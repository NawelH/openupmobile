import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orders-follow-up',
  templateUrl: './orders-follow-up.page.html',
  styleUrls: ['./orders-follow-up.page.scss'],
})
export class OrdersFollowUpPage implements OnInit {

  img = 'https://randomuser.me/api/portraits/women/50.jpg'
  orders = ['10', '27', '39', '151', '375', '1', '2', '5', '7', '8', '45'];
  id = '457';
  status = 'en cours';
  firstname = 'Lisa';
  lastname = 'Liu';
  constructor() { }

  ngOnInit() {
  }

}
