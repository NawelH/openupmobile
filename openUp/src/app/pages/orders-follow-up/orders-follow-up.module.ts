import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrdersFollowUpPage } from './orders-follow-up.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersFollowUpPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrdersFollowUpPage]
})
export class OrdersFollowUpPageModule {}
