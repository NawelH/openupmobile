import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';


const TOKEN_KEY = 'auth-token';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


  authenticationState = new BehaviorSubject(false);

  constructor(private storage: Storage, private plt: Platform,
    private router: Router) { 
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  login() {
    localStorage.setItem('login', 'true');
    // localStorage.setItem('tkn', token);
    return this.storage.set(TOKEN_KEY, 'Bear 123456').then(res => {
      this.authenticationState;
    });
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(()=> {
      this.authenticationState.next(false);
      localStorage.removeItem('login');
      this.router.navigate(['login']);
    });
  }

  
  checkToken() {
    return this.storage.get(TOKEN_KEY).then(res => {
      // if we have a token the user is authenticated
      if(res) {
        this.authenticationState.next(true);
      }
    });
  }
  
  isAuthenticated() {
    return this.authenticationState.value;
  }
}
