import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthenticationService) { }

  canActivate() {
    const login = localStorage.getItem('login');
    if(login === 'true'){
      return true;
    } 
    else {
      // page 403 ???
    }
    // return this.authService.isAuthenticated();
  }
}
