import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private http: HttpClient ) { }

  // get orders by orderer id
  getOrdersByOrderers(id) {
    return this.http.get(`http://127.0.0.1:8000/api/order/closed/orderer/${id}`).toPromise();
   }

  getOrdersByDeliverer(id) {
    return this.http.get(`http://127.0.0.1:8000/api/order/closed/deliverer/${id}`).toPromise();
   }
}
