import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private account;
  private order;

  constructor(private http: HttpClient ) { }

  getDataAccounts() {
   return this.http.get('http://127.0.0.1:8000/api/accounts').toPromise();
  }
  // données accounts users
  getDataAccountsUsers(id) {
   return this.http.get(`http://127.0.0.1:8000/api/users/${id}/account`).toPromise();
  }

  // connexion with email 
  getDataAccount(email) {
   return this.http.post(`http://127.0.0.1:8000/api/account/email/${email}`, '').toPromise();
  }

  getData(id) {
    return this.http.get(`http://127.0.0.1:8000/api/accounts/${id}`).toPromise();
  }
  
  login(form) {
    return this.http.post('http://127.0.0.1:8000/api/login_check', form).toPromise(); 
  }

  setAccount(data) {
    this.account = data;
  }
  getAccount(){
    return this.account;
  }
  //
  getOrders() {
    return this.http.get('http://127.0.0.1:8000/api/orders').toPromise();
  }
  setOrder(dataOrder) {
    this.order = dataOrder;
  }
  getOrder(){
    return this.order;
  }

}
