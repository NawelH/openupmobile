import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(private http: HttpClient) { }

  postRoleUser(form) {
      return this.http.post('http://127.0.0.1:8000/api/accounts', form).toPromise(); 
  }
}
