import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './services/authentication.service';

import { Router } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    //modifications 
    private authService: AuthenticationService,
    private router: Router,
    private authGuard: AuthGuardService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // modification ajout login...
      // this.authService.authenticationState.subscribe(state => {
      //   console.log('Auth changed: ', state)






      // A REMETTRE
      const state = localStorage.getItem('login');
        if(state !== 'true') {
         // this.router.navigate(['profil']); 
          this.router.navigate(['login']);
        } 

         // A REMETTRE





        // else {
        // }
      //});
    });
  }
}
